#!/usr/bin/python
import numpy, socket, time

# initialization for lights in 42
xmit, buff = numpy.zeros(174, 'ubyte'), numpy.zeros((97,3), 'ubyte')
xmit[:8], xmit[20:24] = [4,1,220,74,1,0,8,1], [150,0,255,15]
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
sock.connect(('18.224.0.86', 6038))
def send_packet(data, chan):
    xmit[16], xmit[24:] = chan, data.ravel()
    sock.sendall(xmit)
def write_all():
    send_packet(buff[:50], 2)
    send_packet(buff[:46:-1], 1)

# user-facing methods
def display(pixels):
    """pixels is a 97-by-3 numpy array of floats between 0 and 1"""
    buff[:] = numpy.minimum(256 * numpy.maximum(pixels, 0), 255)
    write_all()


if __name__ == '__main__':
    # display rotating rainbow
    #display(1-numpy.abs(1-(numpy.linspace(0,3,97)[:,None]+time.time()+[0,1,2])%3))
    while True:
        for i in range(97*4):
            display((((numpy.arange(97)+i)[:,None] % 5)==0) + ((numpy.arange(97)<=(i//4))[:,None])) #1d tetris
            time.sleep(0.1)
        for i in range(97*4,0,-1):
            display((((numpy.arange(97)+i)[:,None] % 5)==0) + ((numpy.arange(97)<=(i//4))[:,None])) #1d tetris
            time.sleep(0.1)


